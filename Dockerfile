# build stage
FROM node:14.21.3-alpine3.16 as build-stage

ARG DOCKER_ENV
ENV NODE_ENV=${DOCKER_ENV}

ARG VUE_APP_API_URL_PRODUCTION
ENV VUE_APP_API_URL_PRODUCTION=${VUE_APP_API_URL_PRODUCTION:-https://api.chunk-uploader.hectorbarral.com/api/upload}

ARG VUE_APP_API_URL_DEVELOPMENT
ENV VUE_APP_API_URL_DEVELOPMENT=${VUE_APP_API_URL_DEVELOPMENT:-http://localhost:8000/api/upload}

WORKDIR /app
COPY package*.json /app/
RUN apk update && apk add

RUN node `which npm` install -g @vue/cli && node `which npm` install

COPY . /app/

RUN node `which npm` run build

# production stage
FROM nginx:1.17.9-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/dist /home/admin/web/app/public_html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
