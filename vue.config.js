const autoprefixer = require('autoprefixer')
const tailwindcss = require('tailwindcss')

module.exports = {
  configureWebpack: {
    plugins: [
      tailwindcss,
      autoprefixer
    ]
  }
}
