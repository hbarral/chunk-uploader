module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: [
    './src/**/*.vue'
  ],
  theme: {
    extend: {}
  },
  variants: {},
  plugins: [],
  options: {
    whitelist: [
      'text-green-700',
      'bg-green-100',
      'border-green-300',
      'text-blue-700',
      'bg-blue-100',
      'border-blue-300',
      'text-red-700',
      'bg-red-100',
      'border-red-300',
      'text-yellow-700',
      'bg-yellow-100',
      'border-yellow-300'
    ]
  }
}
